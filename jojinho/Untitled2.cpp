#include "SDL2/SDL.h"
#include "SDL2/SDL_opengl.h"
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_mixer.h"
#include <stdio.h>
#include <conio.h>
#define LARGURA 1024
#define ALTURA 768
#define COLUNA 7
#define LINHA 6
using namespace std;

Mix_Music *music = NULL;
Mix_Chunk *explosao = NULL;

SDL_Window *janelaprincipal = NULL; // nome da janela principal do jogo
SDL_Surface *content = NULL; // �rea de trabalho da janela

SDL_Renderer *visualizacao;

int tabuleiro[LINHA][COLUNA];
typedef struct Teclas{
    bool esquerda;
    bool direita;
    bool espaco;
    bool esc;
} ;

Teclas teclas;

void inicializaTeclas(){
    teclas.esquerda=false;
    teclas.direita=false;
    teclas.espaco=false;
    teclas.esc=false;
}
typedef struct {
    SDL_Rect area; // cria um ret�ngulo area.x, area.y, area.w (largurra), area.h (altura)
    float velocidade; // velocidade que o objeto se move
    SDL_Texture* textura=NULL; // textura da imagem
   float angulo=0;
} Objeto;

Objeto bola;
Objeto bola2;
Objeto bolacaindo;
Objeto grade;
Objeto background;


bool fim=false;

void carregaObjeto(Objeto *o, const char *caminho_da_imagem){
    o->area.w=64; //largura do personagem
    o->area.h=64; //altura do personagem
    SDL_Surface* imagem = IMG_Load(caminho_da_imagem);
    if( imagem == NULL ){
        printf( "Erro ao carregar imagens %s\n", SDL_GetError() );
    }else {
        o->textura = SDL_CreateTextureFromSurface(visualizacao, imagem); // cria a textura
        SDL_FreeSurface(imagem); // apaga a imagem da tela
    }
}
void carregaObjetos()
{
    //Carrega as imagens
    carregaObjeto(&background,"imagens\\xga.png");
    background.area.x=0;
    background.area.y=0;
    background.area.w=LARGURA;
    background.area.h=ALTURA;
    carregaObjeto(&bola,"imagens\\bola3.png");
    bola.area.x=758; //posicao x do meio da janela
    bola.area.y=ALTURA - 768; // posiciona no fim da tela
   bola.velocidade = 2.0f;
   carregaObjeto(&grade,"imagens\\fundo.png");
   grade.area.x=200;
    grade.area.y=155;
    grade.area.w=640;
   grade.area.h=480;
   carregaObjeto(&bolacaindo,"imagens\\bola3.png");
   bolacaindo.area.x=bola.area.x;
   bolacaindo.area.y=bola.velocidade;



}

bool init()
{
    bool success = true;

    //Inicializa a SDL
    if( SDL_Init( SDL_INIT_VIDEO) < 0 )
    {
        printf( "Erro ao carregar a SDL: %s\n", SDL_GetError() );
        success = false;
    }
    else
    {
        //Cria a janela principal
        janelaprincipal = SDL_CreateWindow( "Meu Jogo", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                   LARGURA, ALTURA, SDL_WINDOW_SHOWN );
        if( janelaprincipal == NULL )
        {
            printf( "Erro na cria��o da janela: %s\n", SDL_GetError() );
            success = false;
        }
        else
        {
            visualizacao = SDL_CreateRenderer(janelaprincipal, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
        }




    }
    return success;
}
void desenhaBola(){
    SDL_Point centro;
    centro.x=bola.area.w/2;
    centro.y=bola.area.h/2;
    SDL_RenderCopyEx(visualizacao, bola.textura, NULL, &bola.area, bola.angulo, &centro, SDL_FLIP_NONE );
    //SDL_RenderCopy(visualizacao,nave.textura,NULL, &nave.area);
}
void desenhaBackgound(){
    SDL_RenderCopy(visualizacao,background.textura,NULL, &background.area);
}
void desenhaGrade(){
    SDL_RenderCopy(visualizacao,grade.textura,NULL,&grade.area);

}
void desenharBolacaindo(){
    SDL_Point centro;
    centro.x=bola.area.w/2;
    centro.y=bola.area.h/2;
    SDL_RenderCopy(visualizacao,bolacaindo.textura,NULL,&bolacaindo.area);


}
void close()
{
    SDL_FreeSurface( content );
    content = NULL;
    SDL_DestroyWindow( janelaprincipal );
    janelaprincipal= NULL;
    IMG_Quit();
    SDL_DestroyRenderer(visualizacao);
    SDL_DestroyTexture(bola.textura);

    SDL_DestroyTexture(grade.textura);
    SDL_DestroyTexture(background.textura);
    SDL_Quit(); // fecha a SDL
}

void display(){
    SDL_RenderClear(visualizacao); //limpa a tela
    desenhaBackgound(); // desenha o fundo
    desenhaGrade();
    desenhaBola(); // mostra os objetos

    SDL_RenderPresent(visualizacao);
}

void funcaoEspaco(){

    if(bolacaindo.area.y!=564){

            bolacaindo.area.y+=bola.velocidade;
    }
    else{
            bolacaindo.area.y=564;
        }
}

void executaAcao()
{
    if(teclas.direita){
        if(bola.area.y==ALTURA - 768){
        if(bola.area.x<758){
        bola.area.x+=bola.velocidade;
        }
        else
            bola.area.x=758;
    }}


    if(teclas.esquerda){
        if(bola.area.y==ALTURA - 768){
        if(bola.area.x>218)
        bola.area.x-=bola.velocidade;
        else
            bola.area.x=218;
    }}

    if(teclas.espaco){

            funcaoEspaco();

    }

    if(teclas.esc==true){
        close();
    }

}
void rodadas(){


}

void iniciarJogo(){
inicializaTeclas();
   carregaObjetos();
   SDL_Event evento;
    if( !init() )
    {
        printf( "Falha na inicializa��o!\n" );
    }
    else
    {
        carregaObjetos();
        Mix_PlayMusic( music, -1 );
        explosao = Mix_LoadWAV( "explode.wav" );

        while(!fim){
            while( SDL_PollEvent( &evento ))
            {
                switch(evento.type){
                    case SDL_QUIT :
                        fim = true;
                        break;
                    case SDL_KEYDOWN: // tecla pressionada
                        if(evento.key.keysym.sym == SDLK_LEFT)
                            teclas.esquerda=true;
                            executaAcao();
                        if (evento.key.keysym.sym == SDLK_RIGHT)
                            teclas.direita=true;
                            executaAcao();
                        if(evento.key.keysym.sym == SDLK_ESCAPE)
                            teclas.esc=true;
                            executaAcao();
                        if(evento.key.keysym.sym == SDLK_SPACE)
                            teclas.espaco=true;
                            desenharBolacaindo();
                            executaAcao();
                        break;
                    case SDL_KEYUP: // tecla solta
                        if(evento.key.keysym.sym == SDLK_LEFT)
                            teclas.esquerda=false;
                        if (evento.key.keysym.sym == SDLK_RIGHT)
                            teclas.direita=false;
                        if(evento.key.keysym.sym == SDLK_SPACE)
                            teclas.espaco=false;


                        break;



                }

            }

            display();
        }
    }
}



int main( int argc, char* args[] )
{
iniciarJogo();
}



