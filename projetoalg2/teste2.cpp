#include <iostream>
#include <string>
#include <ctime>
#include "SDL2/SDL.h"
using namespace std;

/* window width and height */
const int WINDOW_WIDTH = 600;
const int WINDOW_HEIGHT = 500;

/* board width and height */
const int BOARD_WIDTH = 10;
const int BOARD_HEIGHT = 5;

/* space between board and edge of the window */
const int OFFSET = 50;

bool add_chip(int board[][BOARD_WIDTH], int col, int val, SDL_Renderer *renderer, int delay);


bool connect_four(const int x[][BOARD_WIDTH], int val);

void winning_board(const int board[][BOARD_WIDTH], SDL_Renderer *renderer, bool player_wins);


void copy_board(const int board[][BOARD_WIDTH], int new_board[][BOARD_WIDTH], int ni, int nj, int val);


int get_col(int xpos);

void draw_board(const int board[][BOARD_WIDTH], SDL_Renderer *renderer);




int main(int argc, char ** argv)
{
	/* Initialize SDL variables */
	SDL_Event event;
	SDL_Window * window = SDL_CreateWindow("Connect Four",
                                           SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, 0);
	SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, 0);
	srand(time(NULL));
    SDL_RenderClear(renderer);


    /* Initialize Connect Four board and draw it */
    int board[BOARD_HEIGHT][BOARD_WIDTH] = {1}; // change to 0 to play
    draw_board(board, renderer);

    bool quit = false;
    bool have_winner = false;
	while (!quit)
	{
        SDL_WaitEvent(&event);

		switch (event.type) {

            case SDL_MOUSEBUTTONDOWN:
                if (!have_winner && event.button.button == SDL_BUTTON_LEFT) {

                    /* get X value of mouse click */
                    int X = event.motion.x;
                    int col = get_col(X);

                    /* if click is not valid, continue until next click */
                    if (col == -1) break;

                    /* add chip to column and draw board */
                    add_chip(board, col, 1, renderer, 300);
                    draw_board(board, renderer);

                    /* check if player wins */
                    have_winner = connect_four(board, 1);

                    /* if player wins, display winning board */
                    if (have_winner) {
                        winning_board(board, renderer, true);
                    } else { // otherwise the computer will move

                        /* TO DO: call function so computer moves */
                        cout << "This is where computer goes...\n";

                        /* check if computer wins */
                        have_winner = connect_four(board, -1);
                        if (have_winner) {
                                winning_board(board, renderer, false);
                        }
                    }
                }
                break;

            /* If player hits the 'n' key */
            case SDL_KEYDOWN:
                if (event.key.keysym.sym == SDLK_n) {
                    cout << "Player presses N" << endl;
                }
                break;
            case SDL_QUIT:
                quit = true;
                break;
		}
	}

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}


bool add_chip(int board[][BOARD_WIDTH], int col, int val,
              SDL_Renderer *renderer, int delay) {

    /* this currently adds chip to the bottom row */
    cout << "in add_chip -- this currently adds chip only to bottom row\n";
    cout << "modify so chip is added to any column\n";
    int i = BOARD_HEIGHT - 1;   // index of last row
    int j = col - 1;            // column index
    int b2[BOARD_HEIGHT][BOARD_WIDTH] = {0};
    copy_board(board, b2, i-1, j, val);
    draw_board(b2, renderer);
    SDL_Delay(delay);
    board[i][j] = val;
    draw_board(board,renderer);
    return true;
}

bool connect_four(const int x[][BOARD_WIDTH], int val) {
    return false;
}


void winning_board(const int board[][BOARD_WIDTH], SDL_Renderer *renderer, bool player_wins) {

    int new_board[BOARD_HEIGHT][BOARD_WIDTH] = {0};
    int val = 1;
    if (!player_wins) val = -1;

    // Currently this randomly fills in up to 200 spots on the board, and then
    // displays the original board. This must be modified to run as described
    // above
    int count = 0;
    while (count < 200) {
        int i = rand() % BOARD_HEIGHT;  // select random row_index
        int j = rand() % BOARD_WIDTH;   // select random column_index
        new_board[i][j] = val;          // add chip to this position
        draw_board(new_board, renderer); // draw the board
        SDL_Delay(10);                  // quick delay
        count++;
    }
    draw_board(board, renderer);    // draw original board
}


// gets the board column from the x position on the window
// if xposition is not on board, function returns -1
int get_col(int xpos) {
    if (xpos < OFFSET || xpos > WINDOW_WIDTH-OFFSET) return -1;
    double chip_width = (WINDOW_WIDTH - 2*OFFSET) / BOARD_WIDTH;
    xpos = xpos - OFFSET;
    return xpos / chip_width + 1;
}


// draws the board; the board array contains the player chips (+1) or
// computer's chips (-1)
void draw_board(const int board[][BOARD_WIDTH], SDL_Renderer *renderer) {

    SDL_Texture * texture =
    SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, WINDOW_WIDTH, WINDOW_HEIGHT);
    SDL_RenderClear(renderer);


    double chip_width = (WINDOW_WIDTH - 2*OFFSET) / BOARD_WIDTH;
    double chip_height = (WINDOW_HEIGHT - 2*OFFSET) / BOARD_HEIGHT;

    Uint32 * pixels = new Uint32[WINDOW_WIDTH * WINDOW_HEIGHT];
	memset(pixels, 255, WINDOW_WIDTH * WINDOW_HEIGHT * sizeof(Uint32));
	SDL_UpdateTexture(texture, NULL, pixels, WINDOW_WIDTH *sizeof(Uint32));
    SDL_RenderCopy(renderer, texture, NULL, NULL);

    // set color to black (r,g,b) = (0,0,0)
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

    // draw vertical lines
    for (int x = OFFSET; x <= WINDOW_WIDTH - OFFSET; x = x + chip_width) {
        SDL_RenderDrawLine(renderer, x,OFFSET, x, WINDOW_HEIGHT-OFFSET);
    }

    // draw horizontal lines
    for (int y = OFFSET; y <= WINDOW_HEIGHT - OFFSET; y =  y + chip_height) {
        SDL_RenderDrawLine(renderer, OFFSET, y, WINDOW_WIDTH-OFFSET,y);
    }

    // color in player chips (black) or computer chips (red)
    for (int i = 0; i < BOARD_HEIGHT; i++) {
        for (int j = 0; j < BOARD_WIDTH; j++) {
            if (board[i][j] != 0) { // there is a chip

                if (board[i][j] == 1) {
                    // set player color to black
                    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
                } else {
                    // set computer to red
                    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
                }

                SDL_Rect rect;
                int off2 = 3;
                rect.x = chip_width*j + OFFSET + off2;
                rect.y = chip_height*i + OFFSET + off2;
                rect.h = chip_height - 2*off2;
                rect.w = chip_width - 2*off2;
                SDL_RenderFillRect(renderer, &rect);
            }
        }
    }

    SDL_RenderPresent(renderer);
}



// copies elements of board into new_board and assigns 'val' to
// new_board[ni][nj]
void copy_board(const int board[][BOARD_WIDTH], int new_board[][BOARD_WIDTH],
                int ni,int nj,int val) {
    for (int i = 0; i < BOARD_HEIGHT; i++) {
        for (int j = 0; j < BOARD_WIDTH; j++) {
            new_board[i][j] = board[i][j];
        }
    }
    new_board[ni][nj] = val;
}
